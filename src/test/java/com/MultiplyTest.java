package com;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;

import java.math.BigDecimal;
import java.math.BigInteger;

class MultiplyTest {
    private final Multiply multiplication = new Multiply();

    @Test
    void testMultiplyMaxByte() {
        short expected = 16129;
        short actual = multiplication.multiplyByte(Byte.MAX_VALUE, Byte.MAX_VALUE);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void testMultiplyMaxShort() {
        int expected = 1073676289;
        int actual = multiplication.multiplyShort(Short.MAX_VALUE, Short.MAX_VALUE);
        assertThat(actual).isEqualTo(expected);
    }
    @Test
    void testMultiplyMaxInt() {
        long expected = 4611686014132420609L;
        long actual = multiplication.multiplyInteger(Integer.MAX_VALUE, Integer.MAX_VALUE);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void testMultiplyMaxLong() {
        BigInteger expected = new BigInteger("85070591730234615847396907784232501249");
        BigInteger actual = multiplication.multiplyLong(Long.MAX_VALUE, Long.MAX_VALUE);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void testMultiplyDecimalWithMaxFloat() {
        BigDecimal expected = new BigDecimal("1.157920754338239133467302250528996E+77");
        BigDecimal actual = multiplication.multiplyDouble(Float.MAX_VALUE, Float.MAX_VALUE);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void testMultiplyDecimalWithMaxDouble() {
        BigDecimal expected = new BigDecimal("3.23170060713109998320439596646649E+616");
        BigDecimal actual = multiplication.multiplyDouble(Double.MAX_VALUE, Double.MAX_VALUE);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
     void testMultiplyShort() {
        int expected = 6;
        int actual = multiplication.multiplyShort((short) 2, (short) 3);
        assertThat(actual).isEqualTo(expected);
    }


    @Test
     void testMultiplyInteger() {
        long expected = 6;
        long actual = multiplication.multiplyInteger(2, 3);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
     void testMultiplyBigInt() {
        BigInteger expected = BigInteger.valueOf(60000000000L);
        BigInteger actual = multiplication.multiplyLong(200000000L, 300L);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
     void testMultiplyDecimal() {
        BigDecimal expected = new BigDecimal("6.00");
        BigDecimal actual = multiplication.multiplyDouble(2.0, 3.0);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void testMultiplyDecimalWithFloat() {
        float first = 2.5f;
        float second = 4.0f;
        BigDecimal expected = BigDecimal.valueOf(first).multiply(BigDecimal.valueOf(second));
        BigDecimal actual = multiplication.multiplyDouble(first, second);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void testMultiplyDecimalWithDouble() {
        double first = 0.1;
        double second = 0.2;
        BigDecimal expected = BigDecimal.valueOf(first).multiply(BigDecimal.valueOf(second));
        BigDecimal actual = multiplication.multiplyDouble(first, second);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void testMultiplyDecimalWithDoubleSecond() {
        double first = 2.00048124d;
        double second = 243126.004124d;
        double expected = 486369.01020622463d;
        BigDecimal result = multiplication.multiplyDouble(first, second);
        assertThat(result.doubleValue()).isEqualTo(expected);
    }
}