package com;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;

import java.io.*;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Properties;
class CalculationTest {
    /*private Calculation calculation;

    @Test
    void testPerformDoubleOperation() throws IOException {
        Properties props = new Properties();
        InputStream inputStream = new FileInputStream("src/test/resources/config.properties");
        props.load(inputStream);

        // Modify the properties as needed
        props.setProperty("min", "1");
        props.setProperty("max", "10");
        props.setProperty("increment", "1");

        // Save the modified properties back to the file
        OutputStream outputStream = new FileOutputStream("src/test/resources/config.properties");
        props.store(outputStream, "Modified by ConfigTest");

        // Close the streams
        inputStream.close();
        outputStream.close();


        Calculation calculation = new Calculation("double");

        calculation.run();
        BigDecimal expected = BigDecimal.valueOf(6.25);
        BigDecimal actual = calculation.bigDecimal;

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void testPerformLongOperation() throws IOException {
        calculation = new Calculation("long");
        calculation.run();
        assertDoesNotThrow(() -> calculation.performLongOperation());

        BigDecimal expected = BigDecimal.valueOf(6);
        BigDecimal actual = calculation.bigDecimal;

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void testPerformIntOperation() throws IOException {
        calculation = new Calculation("int");
        calculation.run();
        assertDoesNotThrow(() -> calculation.performIntOperation());

        BigDecimal expected = BigDecimal.valueOf(2);
        BigDecimal actual = calculation.bigDecimal;

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void testPerformShortOperation() throws IOException {
        calculation = new Calculation("short");
        calculation.run();
        assertDoesNotThrow(() -> calculation.performShortOperation());

        BigDecimal expected = BigDecimal.valueOf(2);
        BigDecimal actual = calculation.bigDecimal;

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void testPerformByteOperation() throws IOException {
        calculation = new Calculation("byte");
        calculation.run();
        assertDoesNotThrow(() -> calculation.performByteOperation());

        BigDecimal expected = BigDecimal.valueOf(2);
        BigDecimal actual = calculation.bigDecimal;

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void testByteOperation() throws IOException {
        Calculation calculation = new Calculation("byte");
        calculation.run();
        assertDoesNotThrow(calculation::performByteOperation);
    }

    @Test
    void testShortOperation() throws IOException {
        calculation = new Calculation("short");
        calculation.run();
        assertDoesNotThrow(calculation::performShortOperation);
    }

    @Test
    void testIntOperation() throws IOException {
        calculation = new Calculation("int");
        calculation.run();
        assertDoesNotThrow(calculation::performIntOperation);
    }

    @Test
    void testLongOperation() throws IOException {
        calculation = new Calculation("long");
        calculation.run();
        assertDoesNotThrow(calculation::performLongOperation);
    }

    @Test
    void testFloatOperation() throws IOException {
        calculation = new Calculation("float");
        calculation.run();
        assertDoesNotThrow(calculation::performFloatOperation);
    }

    @Test
    void testDoubleOperation() throws IOException {
        calculation = new Calculation("double");
        calculation.run();
        assertDoesNotThrow(calculation::performDoubleOperation);
    }

    @Test
    void testRun() throws IOException {
        calculation = new Calculation("byte");
        calculation.run();
        assertDoesNotThrow(calculation::run);
    }*/
}
