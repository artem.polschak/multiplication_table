package com;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import java.io.IOException;

class LoadPropertiesTest {

    private LoadProperties loadProperties;

    @BeforeEach
    void setUp() {
        loadProperties = new LoadProperties();
    }

    @Test
    void getPropertyShouldReturnCorrectValue() throws IOException {
        loadProperties.loadProperties();
        String value = loadProperties.getProperty("min");
        assertThat(value).isNotNull().isEqualTo("1");
    }
    @Test
    void loadPropertiesKeyNotExist() throws IOException {
        loadProperties.loadProperties();
        String value = loadProperties.getProperty("test");
        assertThat(value).isNull();
    }

}
