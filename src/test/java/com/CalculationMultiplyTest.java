package com;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

import java.math.BigDecimal;
import java.math.BigInteger;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;

class CalculationMultiplyTest {

    @Test
    void testByteRun() throws IOException {
        CalculationMultiply calculation = new CalculationMultiply("byte");
        calculation.run();
        assertDoesNotThrow(calculation::run);
    }

    @Test
    void performByteOperationFalse() {
        CalculationMultiply calculation = new CalculationMultiply("byte");
        Assertions.assertFalse(calculation.performByteOperation("127", "127)", "1"));

    }

    @Test
    void performByteOperationThrowExceptionResult() {
        CalculationMultiply calculation = new CalculationMultiply("byte");
        Assertions.assertFalse(calculation.performByteOperation("1", "128)", "1"));

    }

    @Test
    void testByteOperationtTrue() {
        CalculationMultiply calculation = new CalculationMultiply("byte");
        assertTrue(calculation.performByteOperation("1", "127", "1"));
    }

    @Test
    void testByteOperationFalse() {
        CalculationMultiply calculation = new CalculationMultiply("byte");
        Assertions.assertFalse(calculation.performByteOperation("128", "1", "1"));
    }


    @Test
    void testShortRun() throws IOException {
        CalculationMultiply calculation = new CalculationMultiply("short");
        calculation.run();
        assertDoesNotThrow(calculation::run);
    }

    @Test
    void performShortOperationFalse() {
        CalculationMultiply calculation = new CalculationMultiply("short");
        Assertions.assertFalse(calculation.performShortOperation("32767", "32767)", "1"));

    }

    @Test
    void performShortOperationThrowExceptionResult() {
        CalculationMultiply calculation = new CalculationMultiply("short");
        Assertions.assertFalse(calculation.performShortOperation("1", "32768", "1"));

    }

    @Test
    void testShortOperationtTrue() {
        CalculationMultiply calculation = new CalculationMultiply("short");
        assertTrue(calculation.performShortOperation("1", "32767", "1000"));
    }

    @Test
    void testShortOperationFalse() {
        CalculationMultiply calculation = new CalculationMultiply("short");
        Assertions.assertFalse(calculation.performShortOperation("32768", "1", "1"));
    }

    @Test
    void testIntRun() throws IOException {
        CalculationMultiply calculation = new CalculationMultiply("int");
        calculation.run();
        assertDoesNotThrow(calculation::run);
    }

    @Test
    void performIntOperationFalse() {
        CalculationMultiply calculation = new CalculationMultiply("int");
        Assertions.assertFalse(calculation.performIntOperation("2147483647", "2147483647)", "1"));

    }

    @Test
    void performIntOperationThrowExceptionResult() {
        CalculationMultiply calculation = new CalculationMultiply("int");
        Assertions.assertFalse(calculation.performIntOperation("1", "2147483648", "1"));

    }

    @Test
    void testIntOperationtTrue() {
        CalculationMultiply calculation = new CalculationMultiply("int");
        assertTrue(calculation.performIntOperation("1", "100", "10"));
    }

    @Test
    void testPerformLongOperation() throws IOException {

        CalculationMultiply calculation = new CalculationMultiply("long");
        calculation.run();
        BigInteger expected = BigInteger.valueOf(100);
        BigInteger actual = calculation.getBigInteger();

        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void testPerformIntOperation() throws IOException {
        CalculationMultiply calculation = new CalculationMultiply("int");
        calculation.run();
        BigInteger expected = BigInteger.valueOf(100);
        BigInteger actual = calculation.getBigInteger();

        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void testPerformShortOperation() throws IOException {
        CalculationMultiply calculation = new CalculationMultiply("short");
        calculation.run();
        int expected = 100;
        int actual = calculation.getIntResult();

        assertThat(actual).isEqualTo(expected);
    }


    @Test
    void testShortOperation() {
        CalculationMultiply calculation = new CalculationMultiply("short");
        assertDoesNotThrow(() -> {
            calculation.performShortOperation("1", "20", "1");
        });
    }

    @Test
    void testIntOperation() {
        CalculationMultiply calculation = new CalculationMultiply("int");
        assertDoesNotThrow(() -> {
            calculation.performIntOperation("1", "20", "1");
        });
    }

    @Test
    void testLongOperation() {
        CalculationMultiply calculation = new CalculationMultiply("long");
        assertDoesNotThrow(() -> {
            calculation.performLongOperation("1", "20", "1");
        });
    }

    @Test
    void testFloatOperation() {
        CalculationMultiply calculation = new CalculationMultiply("float");
        assertDoesNotThrow(() -> {
            calculation.performFloatOperation("1", "20", "1");
        });
    }

    @Test
    void testDoubleOperation() {
        CalculationMultiply calculation = new CalculationMultiply("double");
        assertDoesNotThrow(() -> {
            calculation.performDoubleOperation("1", "20", "1");
        });
    }
    @Test
    void testDoubleOperationTrue() {
        CalculationMultiply calculation = new CalculationMultiply("double");
        Assertions.assertTrue(calculation.performDoubleOperation("1", "10", "1"));
    }


}
