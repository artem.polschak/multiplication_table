package com.third;

import com.LoadProperties;
import com.LoggerManager;
import org.slf4j.Logger;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.function.BiFunction;

public class CalculationNumber {
    private static final Logger LOGGER = LoggerManager.getLogger();
    private static final String CONFIG_KEY_MIN = "min";
    private static final String CONFIG_KEY_MAX = "max";
    private static final String CONFIG_KEY_INCREMENT = "increment";
    private final LoadProperties loadProperties;

    private BigInteger bigInteger;
    private BigDecimal bigDecimal;
    private final BiFunction<Number, Number, Number> operation;

    private final String type;

    public CalculationNumber(String type) throws IOException {
        this.type = type;
        loadProperties = new LoadProperties();
        loadProperties.loadProperties();

        switch (type) {
            case "byte":
                operation = this::multiplyByte;
                break;
            case "short":
                operation = this::multiplyShort;
                break;
            case "int":
                operation = this::multiplyInt;
                break;
            case "long":
                operation = this::multiplyLong;
                break;
            case "float":
                operation = this::multiplyFloat;
                break;
            case "double":
                operation = this::multiplyDouble;
                break;
            default:
                throw new IllegalArgumentException("Unsupported data type: " + type);
        }
    }

    public void run() {
        long startTime = System.nanoTime();
        performOperation();
        long endTime = System.nanoTime();
        long duration = (endTime - startTime);
        LOGGER.info("Execution time: Multiplication {} {} ns", operation.getClass().getSimpleName(), duration);
    }

    public Number getNumberProperty(String key) throws NumberFormatException {
        String value = loadProperties.getProperty(key);
        if (value == null) {
            throw new NumberFormatException("Missing configuration property: " + key);
        }
        try {
            if (value.contains(".")) {
                return Double.parseDouble(value);
            } else {
                return Long.parseLong(value);
            }
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Invalid configuration property value: " + key + "=" + value);
        }
    }

    private void performOperation() {
        try {
            Number min = getNumberProperty(CONFIG_KEY_MIN);
            Number max = getNumberProperty(CONFIG_KEY_MAX);
            Number increment = getNumberProperty(CONFIG_KEY_INCREMENT);
            Number result;

            if (type.equals("byte")) {
                if (min.shortValue() > Byte.MAX_VALUE
                        || max.shortValue() > Byte.MAX_VALUE
                        || increment.shortValue() > Byte.MAX_VALUE) {
                    LOGGER.error("Error parsing configuration values: {}", max.shortValue());
                    throw new NumberFormatException();
                }
                for (Number i = min; i.doubleValue() <= max.doubleValue(); i = add(i, increment)) {
                    for (Number j = min; j.doubleValue() <= max.doubleValue(); j = add(j, increment)) {
                        result = operation.apply(i, j);

                        if (result.shortValue() > Byte.MAX_VALUE || result.shortValue() < Byte.MIN_VALUE) {
                            //throw new ArithmeticException("Byte overflow: " + i + " * " + j + " = " + result);
                            break;
                        }
                        LOGGER.info("{}x{}={}", i, j, result);
                    }
                }

            } else if (type.equals("short")) {
                for (Number i = min; i.doubleValue() <= max.doubleValue(); i = add(i, increment)) {
                    for (Number j = min; j.doubleValue() <= max.doubleValue(); j = add(j, increment)) {
                        result = operation.apply(i, j);
                        if (result.intValue() > Short.MAX_VALUE || result.intValue() < Short.MIN_VALUE) {
                            //throw new ArithmeticException("Short overflow: " + i + " * " + j + " = " + result);
                            break;
                        }
                        LOGGER.info("{}x{}={}", i, j, result);
                    }
                }
            } else if (type.equals("int")) {
                for (Number i = min; i.doubleValue() <= max.doubleValue(); i = add(i, increment)) {
                    for (Number j = min; j.doubleValue() <= max.doubleValue(); j = add(j, increment)) {
                        result = operation.apply(i, j);

                        if (result.longValue() > Integer.MAX_VALUE || result.longValue() < Integer.MIN_VALUE) {
                            throw new ArithmeticException("Integer overflow: " + i + " * " + j + " = " + result);
                            //break;
                        }
                        LOGGER.info("{}x{}={}", i, j, result);
                    }
                }
            } else if (type.equals("long")) {
                for (Number i = min; i.doubleValue() <= max.doubleValue(); i = add(i, increment)) {
                    for (Number j = min; j.doubleValue() <= max.doubleValue(); j = add(j, increment)) {
                        result = operation.apply(i, j);
                        if (((BigInteger) result).compareTo(BigInteger.valueOf(Long.MAX_VALUE)) > 0 ||
                                ((BigInteger) result).compareTo(BigInteger.valueOf(Long.MIN_VALUE)) < 0) {
                            throw new ArithmeticException("Long overflow: " + i + " * " + j + " = " + result);
                            //break;
                        }
                        LOGGER.info("{}x{}={}", i, j, result);
                    }
                }
            } else if (type.equals("float")) {
                for (Number i = min; i.doubleValue() <= max.doubleValue(); i = add(i, increment)) {
                    for (Number j = min; j.doubleValue() <= max.doubleValue(); j = add(j, increment)) {
                        result = operation.apply(i, j);

                        if (((BigDecimal) result).compareTo(BigDecimal.valueOf(Float.MAX_VALUE)) > 0 ||
                                ((BigDecimal) result).compareTo(BigDecimal.valueOf(Float.MIN_VALUE)) < 0) {
                            throw new ArithmeticException("Float overflow: " + i + " * " + j + " = " + result);
                            //break;
                        }
                        LOGGER.info("{}x{}={}", i, j, result);
                    }
                }

            } else if (type.equals("double")) {
                for (Number i = min; i.doubleValue() <= max.doubleValue(); i = add(i, increment)) {
                    for (Number j = min; j.doubleValue() <= max.doubleValue(); j = add(j, increment)) {
                        result = operation.apply(i, j);
                        if (((BigDecimal) result).compareTo(BigDecimal.valueOf(Double.MAX_VALUE)) > 0 ||
                                ((BigDecimal) result).compareTo(BigDecimal.valueOf(Double.MIN_VALUE)) < 0) {
                            throw new ArithmeticException("Float overflow: " + i + " * " + j + " = " + result);
                            //break;
                        }
                        LOGGER.info("{}x{}={}", i, j, result);
                    }
                }
            }


        } catch (
                NumberFormatException e) {
            LOGGER.error("Error parsing configuration values: {}", e.getMessage());
        }

    }

    private Number add(Number a, Number b) {
        if (a instanceof Byte && b instanceof Byte) {
            return (byte) (a.byteValue() + b.byteValue());
        } else if (a instanceof Short && b instanceof Short) {
            return (short) (a.shortValue() + b.shortValue());
        } else if (a instanceof Integer && b instanceof Integer) {
            return a.intValue() + b.intValue();
        } else if (a instanceof Long && b instanceof Long) {
            return a.longValue() + b.longValue();
        } else if (a instanceof Float && b instanceof Float) {
            return a.floatValue() + b.floatValue();
        } else if (a instanceof Double && b instanceof Double) {
            return a.doubleValue() + b.doubleValue();
        } else {
            throw new IllegalArgumentException("Unsupported data type: " + a.getClass().getSimpleName() + ", " + b.getClass().getSimpleName());
        }
    }

    private Number multiplyByte(Number a, Number b) {
        return a.shortValue() * b.shortValue();
    }

    private Number multiplyShort(Number a, Number b) {
        return a.intValue() * b.intValue();
    }

    private Number multiplyInt(Number a, Number b) {
        return BigInteger.valueOf(a.intValue()).multiply(BigInteger.valueOf(b.intValue()));
    }

    private Number multiplyLong(Number a, Number b) {
        return BigInteger.valueOf(a.longValue()).multiply(BigInteger.valueOf(b.longValue()));
    }

    private Number multiplyFloat(Number a, Number b) {
        return BigDecimal.valueOf(a.floatValue()).multiply(BigDecimal.valueOf(b.floatValue()));
    }

    private Number multiplyDouble(Number a, Number b) {
        return BigDecimal.valueOf(a.doubleValue()).multiply(BigDecimal.valueOf(b.doubleValue()));
    }
}
