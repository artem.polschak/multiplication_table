package com;

import org.slf4j.Logger;

import java.io.IOException;

public class Main {
    private static final Logger LOGGER = LoggerManager.getLogger();
    private static final  String TYPE = "type";
    public static void main(String[] args) throws IOException {
        CalculationMultiply calculator = new CalculationMultiply(returnDType());
        calculator.run();
        LOGGER.info("finish the program");


    }
    public static String returnDType() {
        return System.getProperty(TYPE, "byte");
    }
}
