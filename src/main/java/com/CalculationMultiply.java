package com;

import org.slf4j.Logger;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

public class CalculationMultiply {
    private static final Logger LOGGER = LoggerManager.getLogger();
    private static final String CONFIG_KEY_MIN = "min";
    private static final String CONFIG_KEY_MAX = "max";
    private static final String CONFIG_KEY_INCREMENT = "increment";
    private short shortResult;
    private int intResult;
    private BigInteger bigInteger;
    private BigDecimal bigDecimal;
    private final String type;
    private String valueMin;
    private String valueMax;
    private String valueIncrement;

    public int getIntResult() {
        return intResult;
    }

    public BigInteger getBigInteger() {
        return bigInteger;
    }

    private final Multiply multiplyObject = new Multiply();

    public CalculationMultiply(String type) {
        this.type = type;
    }

    public void run() throws IOException {
        LoadProperties loadProperties;
        loadProperties = new LoadProperties();
        loadProperties.loadProperties();
        valueMin = loadProperties.getProperty(CONFIG_KEY_MIN);
        valueMax = loadProperties.getProperty(CONFIG_KEY_MAX);
        valueIncrement = loadProperties.getProperty(CONFIG_KEY_INCREMENT);

        performOperation();
    }
    public void performOperation() {
        long startTime = System.nanoTime();
        switch (type) {
            case "byte":
                performByteOperation(valueMin, valueMax, valueIncrement);
                break;
            case "short":
                performShortOperation(valueMin, valueMax, valueIncrement);
                break;
            case "int":
                performIntOperation(valueMin, valueMax, valueIncrement);
                break;
            case "long":
                performLongOperation(valueMin, valueMax, valueIncrement);
                break;
            case "float":
                performFloatOperation(valueMin, valueMax, valueIncrement);
                break;
            case "double":
                performDoubleOperation(valueMin, valueMax, valueIncrement);
                break;
            default:
                LOGGER.error("Unsupported data type");
                return;
        }
        long endTime = System.nanoTime();
        long duration = (endTime - startTime);
        LOGGER.info("Execution time: Multiplication {} {} ns", type, duration);
    }

    public boolean performByteOperation(String valueMin, String valueMax, String valueIncrement) {
        try {
            byte min = Byte.parseByte(valueMin);
            byte max = Byte.parseByte(valueMax);
            byte increment = Byte.parseByte(valueIncrement);
            LOGGER.info("Byte min value={}, max value={}, increment={}", min, max, increment);

            int result = multiplyObject.multiplyShort(min, max);

            if (result > Byte.MAX_VALUE || result < Byte.MIN_VALUE) {
                LOGGER.error("multiplication byte: {}x{}={} out of byte size {}", min, max, result, Byte.MAX_VALUE );
                throw new NumberFormatException("Multiplication result exceeds byte range");
            }

            for (short i = min; i <= max; i += increment) {
                for (short j = min; j <= max; j += increment) {
                    shortResult = multiplyObject.multiplyByte(i, j);
                    if (shortResult > Byte.MAX_VALUE) {
                        break;
                    }
                    LOGGER.info("multiplication byte: {}x{}={}", i, j, shortResult);
                }
            }
            return true;
        } catch (NumberFormatException e) {
            LOGGER.error("one of param from configurations file are out of Byte size range: {}", e.getMessage(), e);
            return false;
        }
    }

    public boolean performShortOperation(String valueMin, String valueMax, String valueIncrement) {
        try {
            short min = Short.parseShort(valueMin);
            short max = Short.parseShort(valueMax);
            short increment = Short.parseShort(valueIncrement);
            LOGGER.info("Short min value={}, max value={}, increment={}", min, max, increment);

            int result = min * max;

            if (result > Short.MAX_VALUE || result < Short.MIN_VALUE) {
                LOGGER.error("multiplication short: {}x{}={} out of short size {}", min, max, result, Short.MAX_VALUE );
                throw new NumberFormatException("Multiplication result exceeds short range");
            }

            for (int i = min; i <= max; i += increment) {
                for (int j = min; j <= max; j += increment) {
                    intResult = multiplyObject.multiplyShort(i, j);
                    if (intResult > Short.MAX_VALUE) {
                        break;
                    }
                    LOGGER.info("multiplication short: {}x{}={}", i, j, intResult);
                }
            }
            return true;
        } catch (NumberFormatException e) {
            LOGGER.error("one of param from configurations file are out of Short size range: {}", e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    public boolean performIntOperation(String valueMin, String valueMax, String valueIncrement) {
        try {
            int min = Integer.parseInt(valueMin);
            int max = Integer.parseInt(valueMax);
            int increment = Integer.parseInt(valueIncrement);
            LOGGER.info("Integer min value={}, max value={}, increment={}", min, max, increment);

            BigInteger result = BigInteger.valueOf(min).multiply(BigInteger.valueOf(max));
            if (result.compareTo(BigInteger.valueOf(Integer.MAX_VALUE)) > 0
                    || result.compareTo(BigInteger.valueOf(Integer.MIN_VALUE)) < 0) {
                LOGGER.error("Multiplication result of {}x{} exceeds Integer range", min, max);
                throw new NumberFormatException("Multiplication result exceeds Integer range");
            }

            for (int i = min; i <= max; i += increment) {
                for (int j = min; j <= max; j += increment) {
                    bigInteger = multiplyObject.multiplyLong(i, j);
                    if (bigInteger.compareTo(BigInteger.valueOf(Integer.MAX_VALUE)) > 0) {
                        break;
                    }
                    LOGGER.info("multiplication Integer: {}x{}={}", i, j, bigInteger);
                }
            }
            return true;
        } catch (NumberFormatException e) {
            LOGGER.error("one of param from file config.properties are out of Integer size range: {}", e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    public boolean performLongOperation(String valueMin, String valueMax, String valueIncrement) {
        try {
            long min = Long.parseLong(valueMin);
            long max = Long.parseLong(valueMax);
            long increment = Long.parseLong(valueIncrement);
            LOGGER.info("Long min value={}, max value={}, increment={}", min, max, increment);

            BigInteger result = BigInteger.valueOf(min).multiply(BigInteger.valueOf(max));
            if (result.compareTo(BigInteger.valueOf(Long.MAX_VALUE)) > 0
                    || result.compareTo(BigInteger.valueOf(Long.MIN_VALUE)) < 0) {
                LOGGER.error("Multiplication result of {}x{} exceeds long range", min, max);
                throw new NumberFormatException("Multiplication result exceeds long range");
            }

            for (long i = min; i <= max; i += increment) {
                for (long j = min; j <= max; j += increment) {
                    bigInteger = multiplyObject.multiplyLong(i, j);
                    if (bigInteger.compareTo(BigInteger.valueOf(Long.MAX_VALUE)) > 0) {
                        break;
                    }
                    LOGGER.info("multiplication long: {}x{}={}", i, j, bigInteger);
                }
            }
            return true;
        } catch (NumberFormatException e) {
            LOGGER.error("one of param from file config.properties are out of Long size range: {}", e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    public boolean performFloatOperation(String valueMin, String valueMax, String valueIncrement) {
        try {
            float min = Float.parseFloat(valueMin);
            float max = Float.parseFloat(valueMax);
            float increment = Float.parseFloat(valueIncrement);

            LOGGER.info("Float min value={}, max value={}, increment={}", min, max, increment);

            BigDecimal result = BigDecimal.valueOf(min).multiply(BigDecimal.valueOf(max));
            if (result.compareTo(BigDecimal.valueOf(Float.MAX_VALUE)) > 0
                    || result.compareTo(BigDecimal.valueOf(Float.MIN_VALUE)) < 0) {
                LOGGER.error("Multiplication result of {}x{} exceeds Float range", min, max);
                throw new NumberFormatException("Multiplication result exceeds Float range");
            }

            for (double i = min; i <= max; i += increment) {
                for (double j = min; j <= max; j += increment) {
                    bigDecimal = multiplyObject.multiplyFloat(i, j);
                    if (bigDecimal.compareTo(BigDecimal.valueOf(Float.MAX_VALUE)) > 0) {
                        break;
                    }
                    LOGGER.info("multiplication float: {}x{}={}", i, j, bigDecimal);
                }
            }
            return true;
        } catch (NumberFormatException e) {
            LOGGER.error("one of param from file config.properties are out of Float size range: {}", e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    public boolean performDoubleOperation(String valueMin, String valueMax, String valueIncrement) {
        try {
            double min = Double.parseDouble(valueMin);
            double max = Double.parseDouble(valueMax);
            double increment = Double.parseDouble(valueIncrement);

            LOGGER.info("Double min value={}, max value={}, increment={}", min, max, increment);

            BigDecimal result = BigDecimal.valueOf(min).multiply(BigDecimal.valueOf(max));
            if (result.compareTo(BigDecimal.valueOf(Double.MAX_VALUE)) > 0
                    || result.compareTo(BigDecimal.valueOf(Double.MIN_VALUE)) < 0) {
                LOGGER.error("Multiplication result of {}x{} exceeds Double range", min, max);
                throw new NumberFormatException("Multiplication result exceeds Double range");
            }

            for (double i = min; i <= max; i += increment) {
                for (double j = min; j <= max; j += increment) {
                    bigDecimal = multiplyObject.multiplyDouble(i, j);
                    if (bigDecimal.compareTo(BigDecimal.valueOf(Double.MAX_VALUE)) > 0) {
                        break;
                    }
                    LOGGER.info("multiplication double: {}x{}={}", i, j, bigDecimal);
                }
            }
            return true;
        } catch (NumberFormatException e) {
            LOGGER.error("one of param from file config.properties are out of Double size range: {}", e.getMessage(), e);
            
            return false;
        }
    }
}
