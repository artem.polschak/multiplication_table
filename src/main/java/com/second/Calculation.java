package com.second;

import com.LoadProperties;
import com.LoggerManager;
import org.slf4j.Logger;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;


public class Calculation {
    private static final Logger LOGGER = LoggerManager.getLogger();
    private static final  String CONFIG_KEY_MIN = "min";
    private static final String CONFIG_KEY_MAX = "max";
    private static final String CONFIG_KEY_INCREMENT = "increment";
    private LoadProperties loadProperties;
     BigDecimal bigDecimal;
    BigInteger bigInteger;
    private final String type;


    public Calculation(String type) {
        this.type = type;
    }
    public void run() throws IOException {
        loadProperties = new LoadProperties();
        loadProperties.loadProperties();

        performOperation();
    }
    public void performOperation() {
        long startTime = System.nanoTime();
        switch (type) {
            case "byte":
                performByteOperation();
                break;
            case "short":
                performShortOperation();
                break;
            case "int":
                performIntOperation();
                break;
            case "long":
                performLongOperation();
                break;
            case "float":
                performFloatOperation();
                break;
            case "double":
                performDoubleOperation();
                break;
            default:
                LOGGER.error("Unsupported data type");
                return;
        }
        long endTime = System.nanoTime();
        long duration = (endTime - startTime);
        LOGGER.info("Execution time: Multiplication {} {} ns", type, duration);
    }
     void performByteOperation() {
        try {
            byte min = Byte.parseByte(loadProperties.getProperty(CONFIG_KEY_MIN));
            byte max = Byte.parseByte(loadProperties.getProperty(CONFIG_KEY_MAX));
            byte increment = Byte.parseByte(loadProperties.getProperty(CONFIG_KEY_INCREMENT));

            for (short i = min; i <= max; i += increment) {
                for (short j = min; j <= max; j += increment) {
                    LOGGER.info("multiplication byte: {}x{}={}", i, j, i * j);
                }
            }
        } catch (NumberFormatException e) {
            LOGGER.error("Error parsing configuration byte values: {}", e.getMessage());
        }
    }

     void performShortOperation() {
        try {
            short min = Short.parseShort(loadProperties.getProperty(CONFIG_KEY_MIN));
            short max = Short.parseShort(loadProperties.getProperty(CONFIG_KEY_MAX));
            short increment = Short.parseShort(loadProperties.getProperty(CONFIG_KEY_INCREMENT));

            for (int i = min; i <= max; i += increment) {
                for (int j = min; j <= max; j += increment) {
                    LOGGER.info("multiplication short: {}x{}={}", i, j, i * j);
                }
            }
        } catch (NumberFormatException e) {
            LOGGER.error("Error parsing configuration short values: {}", e.getMessage());
        }
    }

     void performIntOperation() {
        try {
            int min = Integer.parseInt(loadProperties.getProperty(CONFIG_KEY_MIN));
            int max = Integer.parseInt(loadProperties.getProperty(CONFIG_KEY_MAX));
            int increment = Integer.parseInt(loadProperties.getProperty(CONFIG_KEY_INCREMENT));

            for (long i = min; i <= max; i += increment) {
                for (long j = min; j <= max; j += increment) {
                    bigInteger = BigInteger.valueOf(i * j);
                    LOGGER.info("multiplication int: {}x{}={}", i, j,  bigInteger);
                }
            }
        } catch (NumberFormatException e) {
            LOGGER.error("Error parsing configuration int values: {}", e.getMessage());
        }
    }

     void performLongOperation() {
        try {
            long min = Long.parseLong(loadProperties.getProperty(CONFIG_KEY_MIN));
            long max = Long.parseLong(loadProperties.getProperty(CONFIG_KEY_MAX));
            long increment = Long.parseLong(loadProperties.getProperty(CONFIG_KEY_INCREMENT));

            for (long i = min; i < max; ) {
                if (i + increment > max) {
                    break;
                } else {
                    i += increment;
                }
                for (long j = min; j < max; ) {
                    if (j + increment > max) {
                        break;
                    } else {
                        j += increment;
                    }
                    bigInteger = BigInteger.valueOf(i).multiply(BigInteger.valueOf(j));
                    LOGGER.info("multiplication long: {}x{}={}", i, j, bigInteger);
                }
            }
        } catch (NumberFormatException e) {
            LOGGER.error("Error parsing configuration long values: {}", e.getMessage());
        }
    }
     void performFloatOperation() {
        try {
            float min = Float.parseFloat(loadProperties.getProperty(CONFIG_KEY_MIN));
            float max = Float.parseFloat(loadProperties.getProperty(CONFIG_KEY_MAX));
            float increment = Float.parseFloat(loadProperties.getProperty(CONFIG_KEY_INCREMENT));

            for (double i = min; i < max; ) {
                if (i + increment > max) {
                    break;
                } else {
                    i += increment;
                }
                for (double j = min; j < max; ) {
                    if (j + increment > max) {
                        break;
                    } else {
                        j += increment;
                    }
                    bigDecimal = BigDecimal.valueOf(i).multiply(BigDecimal.valueOf(j));
                    LOGGER.info("multiplication float: {}x{}={}", i, j, bigDecimal);
                }
            }
        } catch (NumberFormatException e) {
            LOGGER.error("Error parsing configuration values: {}", e.getMessage());
        }
    }
     void performDoubleOperation() {
        try {
            double min = Double.parseDouble(loadProperties.getProperty(CONFIG_KEY_MIN));
            double max = Double.parseDouble(loadProperties.getProperty(CONFIG_KEY_MAX));
            double increment = Double.parseDouble(loadProperties.getProperty(CONFIG_KEY_INCREMENT));

            for (double i = min; i <= max; i += increment) {
                if (i  > max) {
                    break;
                }
                for (double j = min; j < max; ) {
                    if (j + increment > max) {
                        break;
                    } else {
                        bigDecimal = BigDecimal.valueOf(i).multiply(BigDecimal.valueOf(j));
                        LOGGER.info("multiplication double: {}x{}={}", i, j, bigDecimal);
                        j += increment;
                    }

                }
            }
        } catch (NumberFormatException e) {
            LOGGER.error("Error parsing configuration values: {}", e.getMessage());
        }
    }
}
