package com;

import java.math.BigDecimal;
import java.math.BigInteger;

public class Multiply {

    public short multiplyByte(short first, short second) {
        int result = first * second;
        return (short) result;
    }
    public int multiplyShort(int first, int second) {
        return first * second;
    }

    public long multiplyInteger(long first, long second) {
        return first * second;
    }

    public BigInteger multiplyLong(long first, long second) {
        return BigInteger.valueOf(first).multiply(BigInteger.valueOf(second));
    }

    public BigDecimal multiplyFloat(double first, double second) {
        return BigDecimal.valueOf(first).multiply(BigDecimal.valueOf(second));
    }

    public BigDecimal multiplyDouble(double first, double second) {
        return BigDecimal.valueOf(first).multiply(BigDecimal.valueOf(second));
    }
}
