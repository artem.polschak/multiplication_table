package com;

import org.slf4j.Logger;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class LoadProperties {
    private static final Logger LOGGER = LoggerManager.getLogger();
    private static final String DEFAULT_CONFIG = "config.properties";
    private static final String EXTERNAL_CONFIG = "../config.properties";
    private Properties properties;

    public String getProperty(String key) {
        return properties.getProperty(key);
    }

    /**
     * This method loads the config file from an external location if it exists,
     * otherwise it loads from the default location.
     * @throws IOException if an error occurs while reading the configuration file
     */
    public void loadProperties() throws IOException {
        File externalConfigFile = new File(EXTERNAL_CONFIG);
        properties = new Properties();
        if (externalConfigFile.exists()) {
            LOGGER.info("External config file {} exists, loading properties from there", externalConfigFile);
            try (FileInputStream file = new FileInputStream(externalConfigFile);
                 InputStreamReader reader = new InputStreamReader(file, StandardCharsets.UTF_8)) {
                properties.load(reader);
            } catch (IOException e) {
                LOGGER.error("Failed to load config from external location: {}", EXTERNAL_CONFIG, e);
            }
        } else {
            LOGGER.info("External config file not found, loading properties from default location");
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(DEFAULT_CONFIG);
            if (inputStream != null) {
                try (InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8)) {
                    properties.load(reader);
                } catch (IOException e) {
                    LOGGER.error("Failed to load config from classpath: {}", DEFAULT_CONFIG, e);
                    throw e;
                }
            } else {
                String errorMsg = "Config file not found.";
                LOGGER.error(errorMsg);
                throw new FileNotFoundException(errorMsg);
            }
        }
    }
}
